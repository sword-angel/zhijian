package com.offcn.order.pojo;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
@Data
@ToString
public class TTransaction implements Serializable {

    private Integer id;

    private String ordersn;

    private String paysn;

    private Integer memberid;

    private Float amount;

    private Byte paystate;

    private String source;

    private Byte status;

    private String completiontime;

    private String note;

    private String createat;

    private String updateat;


}