package com.offcn.order.service.impl;

import com.offcn.dycommon.enums.OrderStatusEnumes;
import com.offcn.dycommon.response.AppResponse;
import com.offcn.order.mapper.TOrderMapper;
import com.offcn.order.pojo.TOrder;
import com.offcn.order.service.OrderService;
import com.offcn.order.service.ProjectServiceFeign;
import com.offcn.order.vo.req.OrderInfoSubmitVo;
import com.offcn.order.vo.req.TReturn;
import com.offcn.util.AppDateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * @Auther:Justc
 * @Date:2021/5/17 17:28
 * @Description
 */
@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private TOrderMapper orderMapper;
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private ProjectServiceFeign projectServiceFeign;

    /**
     * 保存订单
     * @param vo
     * @return
     */
    @Override
    public TOrder saveOrder(OrderInfoSubmitVo vo) {
        TOrder order = new TOrder();
        //获得令牌
        String accessToken = vo.getAccessToken();
        //从缓存中得到会员id
        String memberId = redisTemplate.opsForValue().get(accessToken);
        order.setMemberid(Integer.parseInt(memberId));
        //项目id
        order.setProjectid(vo.getProjectid());
        //回报项目ID可以有多个默认的那个
        order.setReturnid(vo.getReturnid());
        //生成订单编号
        String orderNum = UUID.randomUUID().toString().replaceAll("-", "");
        order.setOrdernum(orderNum);
        //设置订单创建时间
        order.setCreatedate(AppDateUtils.getFormatTime());
        //从项目模块中得到回报列表
        AppResponse<List<TReturn>> appResponse= projectServiceFeign.detailsReturn(vo.getProjectid());
        List<TReturn> returnList = appResponse.getData();
        //取出第一个回报的金额作为订单金额
        TReturn tReturn = returnList.get(0);
        //计算出支持金额
        int totalMoney = tReturn.getSupportmoney() * vo.getRtncount() + tReturn.getFreight();
        order.setMoney(totalMoney);
        //设置回报数量
        order.setRtncount(vo.getRtncount());
        //支付状态
        order.setStatus(OrderStatusEnumes.UNPAY.getCode() + "");
        //收货地址
        order.setAddress(vo.getAddress());
        //是否开发票
        order.setInvoice(vo.getInvoice().toString());
        //发票抬头
        order.setInvoictitle(vo.getInvoictitle());
        //备注
        order.setRemark(vo.getRemark());
        orderMapper.insertSelective(order);
        return order;
    }
}
