package com.offcn.user.exception;

import com.offcn.user.enums.UserExceptionEnum;

/**
 * @Auther:Justc
 * @Date:2021/5/12 22:22
 * @Description
 * 用户异常封装类
 */
public class UserException extends RuntimeException {
    public UserException(UserExceptionEnum exceptionEnum) {
        super(exceptionEnum.getMsg());
    }
}
