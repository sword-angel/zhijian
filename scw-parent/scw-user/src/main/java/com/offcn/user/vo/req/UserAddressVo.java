package com.offcn.user.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Auther:Justc
 * @Date:2021/5/17 20:33
 * @Description
 */

@ApiModel
@Data

public class UserAddressVo {
    @ApiModelProperty("地址id")
    private Integer id ;

    @ApiModelProperty("会员地址")
    private String address ;

}
