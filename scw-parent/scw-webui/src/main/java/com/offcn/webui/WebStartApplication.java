package com.offcn.webui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Auther:Justc
 * @Date:2021/5/19 16:34
 * @Description
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)  //不需要访问数据库,排除数据库连接池的自动配置
@EnableDiscoveryClient
@EnableFeignClients
@EnableCircuitBreaker
public class WebStartApplication {
    public static void main(String[] args) {
        SpringApplication.run(WebStartApplication.class);
    }
}



