package com.offcn.project.service;

import com.offcn.project.enums.ProjectStatusEnume;
import com.offcn.project.vo.req.ProjectRedisStorageVo;

/**
 * @Auther:Justc
 * @Date:2021/5/14 14:04
 * @Description
 */
public interface ProjectCreateService {
    //初始化项目
    public String initCreateProject(Integer memberId);


    /**
     * 保存项目信息
     * @param auth  项目状态信息
     * @param project  项目全部信息
     */
    public void saveProjectInfo(ProjectStatusEnume auth, ProjectRedisStorageVo project);
}
