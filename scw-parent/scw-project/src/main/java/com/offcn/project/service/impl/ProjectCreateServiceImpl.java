package com.offcn.project.service.impl;

import com.alibaba.fastjson.JSON;
import com.offcn.project.contants.ProjectConstant;
import com.offcn.project.enums.ProjectImageTypeEnume;
import com.offcn.project.enums.ProjectStatusEnume;
import com.offcn.project.mapper.*;
import com.offcn.project.pojo.*;
import com.offcn.project.service.ProjectCreateService;
import com.offcn.project.vo.req.ProjectRedisStorageVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @Auther:Justc
 * @Date:2021/5/14 14:07
 * @Description
 */
@Service
public class ProjectCreateServiceImpl implements ProjectCreateService {
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private TProjectMapper projectMapper;
    @Autowired
    private TProjectImagesMapper projectImagesMapper;

    @Autowired
    private TProjectTagMapper projectTagMapper;

    @Autowired
    private TProjectTypeMapper projectTypeMapper;

    @Autowired
    private TReturnMapper tReturnMapper;

    /**
     * 初始化项目
     * @param memberId
     * @return 令牌
     */
    @Override
    public String initCreateProject(Integer memberId) {
        //生成一个令牌
        String token = UUID.randomUUID().toString().replaceAll("-", "");
        //项目的临时对象
        ProjectRedisStorageVo initVo = new ProjectRedisStorageVo();
        initVo.setMemberid(memberId);
        //将initVo转换成JSON字符串
        String jsonString = JSON.toJSONString(initVo);
        //存储到redis中
        redisTemplate.opsForValue().set(ProjectConstant.TEMP_PROJECT_PREFIX + token, jsonString);
        return token;
    }

    /**
     *保存项目信息
     * @param auth  项目状态信息
     * @param storageVo  项目全部信息
     */
    @Override
    public void saveProjectInfo(ProjectStatusEnume auth, ProjectRedisStorageVo storageVo) {
        //1.保存项目信息
        TProject project = new TProject();
        //将storageVo中包含project的信息同步过去
        BeanUtils.copyProperties(storageVo, project);
        project.setCreatedate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        //基本的信息插入，获取到刚才保存好的项目的自增id
        projectMapper.insertSelective(project);  //insrtSelective包含动态sql

        //取出项目生成的id
        Integer projectId = project.getId();

        //2.保存图片信息(分为头图片和详情图片)
        //2.1保存头图片
        String headerImage = storageVo.getHeaderImage();
        TProjectImages projectImages = new TProjectImages(projectId, headerImage, ProjectImageTypeEnume.HEADER.getCode());
        projectImagesMapper.insertSelective(projectImages);
        //2.2保存详情图
        List<String> detailsImage = storageVo.getDetailsImage();
        for (String img : detailsImage) {
            TProjectImages detaImg = new TProjectImages(projectId, img, ProjectImageTypeEnume.DETAILS.getCode());
            projectImagesMapper.insertSelective(detaImg);
        }


        //3.保存项目标签信息
        List<Integer> tagids = storageVo.getTagids();
        for (Integer tagid : tagids) {
            TProjectTag tProjectTag = new TProjectTag(projectId, tagid);
            projectTagMapper.insertSelective(tProjectTag);
        }

        //4.保存项目分类信息
        List<Integer> typeids = storageVo.getTypeids();
        for (Integer typeid : typeids) {
            TProjectType tProjectType = new TProjectType(projectId, typeid);
            projectTypeMapper.insertSelective(tProjectType);
        }

        //5.保存回报信息
        List<TReturn> projectReturns = storageVo.getProjectReturns();
        for (TReturn tReturn : projectReturns) {
            tReturn.setProjectid(projectId);
            tReturnMapper.insertSelective(tReturn);
        }


        //6.删除临时数据
        redisTemplate.delete(ProjectConstant.TEMP_PROJECT_PREFIX + storageVo.getProjectToken());

    }
}
