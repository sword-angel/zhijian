package com.offcn.project.config;

import com.offcn.util.OssTemplate;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Auther:Justc
 * @Date:2021/5/13 15:58
 * @Description
 */
@Configuration
public class AppProjectConfig {
    //加载配置文件中的oss属性
    @ConfigurationProperties(prefix = "oss")
    //注入bean OSSTemplate
    @Bean
    public OssTemplate ossTemplate(){
        return new OssTemplate();
    }
}
