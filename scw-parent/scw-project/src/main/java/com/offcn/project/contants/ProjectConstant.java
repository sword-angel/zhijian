package com.offcn.project.contants;

/**
 * @Auther:Justc
 * @Date:2021/5/14 13:59
 * @Description
 */
public class ProjectConstant {
    //拼接在projectToken前。用于redis中的key
    public static final String TEMP_PROJECT_PREFIX = "project:create:temp:";

}
