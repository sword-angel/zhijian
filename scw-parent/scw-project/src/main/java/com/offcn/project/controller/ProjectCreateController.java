package com.offcn.project.controller;

import com.alibaba.fastjson.JSON;
import com.offcn.dycommon.response.AppResponse;
import com.offcn.project.contants.ProjectConstant;
import com.offcn.project.enums.ProjectStatusEnume;
import com.offcn.project.pojo.TReturn;
import com.offcn.project.service.ProjectCreateService;
import com.offcn.project.vo.req.ProjectBaseInfoVo;
import com.offcn.project.vo.req.ProjectRedisStorageVo;
import com.offcn.project.vo.req.ProjectReturnVo;
import com.offcn.vo.BaseVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther:Justc
 * @Date:2021/5/14 14:31
 * @Description
 */
@RestController
@Slf4j
@Api(tags = "项目基本功能模块（创建、保存、项目信息获取等）")
@RequestMapping("/project")
public class ProjectCreateController {

    @Autowired
    private ProjectCreateService projectCreateService;

    @Autowired
    private StringRedisTemplate redisTemplate;


    @ApiOperation("项目发起第一步-阅读同意相关协议")
    @GetMapping("/init")
    public AppResponse<Object> init(BaseVo vo) {
        String accessToken = vo.getAccessToken(); //获取令牌
        //根据登录令牌获取项目id
        String memberId = redisTemplate.opsForValue().get(accessToken);
        if (StringUtils.isEmpty(memberId)) {
            return AppResponse.fail("无此权限,请先登录");
        }
        int id = Integer.parseInt(memberId);
        //保存临时信息到redis
        String projectToken = projectCreateService.initCreateProject(id);
        return AppResponse.ok(projectToken);


    }


    @ApiOperation("项目发起第2步-保存项目的基本信息")
    @PostMapping("/savebaseInfo")
    public AppResponse<String> savebaseInfo( ProjectBaseInfoVo vo) {

        //1.取得redis中存储的基本信息
        String jsonString = redisTemplate.opsForValue().get(ProjectConstant.TEMP_PROJECT_PREFIX + vo.getProjectToken());

        //2.将json字符串转换为对象
        ProjectRedisStorageVo storageVo = JSON.parseObject(jsonString, ProjectRedisStorageVo.class);

        //3.将页面采集的数据分装到stroageVo中
        BeanUtils.copyProperties(vo, storageVo);

        //4. 将这个vo对象在转换成Json字符串存到redis中
        String storageVoString = JSON.toJSONString(storageVo);
        redisTemplate.opsForValue().set(ProjectConstant.TEMP_PROJECT_PREFIX + vo.getProjectToken(), storageVoString);
        return AppResponse.ok("OK");


    }
    @ApiOperation("项目发起第3步-项目保存项目回报信息")
    @PostMapping("/savereturn")
    public  AppResponse<Object> saveReturnInfo(@RequestBody List<ProjectReturnVo> pro) {
        //1.取出一个对象
        ProjectReturnVo vo = pro.get(0);
        //2.取出项目临时令牌
        String token = vo.getProjectToken();
        //3.取出项目信息
        String jsonString = redisTemplate.opsForValue().get(ProjectConstant.TEMP_PROJECT_PREFIX + token);
        ProjectRedisStorageVo storageVo = JSON.parseObject(jsonString, ProjectRedisStorageVo.class);
        //4. 将List<ProjectReturnVo>包装类型集合转换为List<TReturn>实体类型
        List<TReturn> returns = new ArrayList<>();
        for (ProjectReturnVo returnVo : pro) {
            TReturn tReturn = new TReturn();
            BeanUtils.copyProperties(returnVo, tReturn);
            returns.add(tReturn);
        }
        //5.更新return集合
        storageVo.setProjectReturns(returns);
        String voString = JSON.toJSONString(storageVo);
        //6. 保存到redis中
        redisTemplate.opsForValue().set(ProjectConstant.TEMP_PROJECT_PREFIX + token, voString);
        return AppResponse.ok("保存项目回报信息完成 ");
    }


    @ApiOperation("项目发起第4步-保存项目到数据库中")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "accessToken", value = "用户令牌", required = true),
            @ApiImplicitParam(name = "projectToken",value = "项目标识",required = true),
            @ApiImplicitParam(name = "ops",value = "用户操作类型 0-保存草稿 1-提交审核",required = true)
    })

    @PostMapping("/submit")
    public AppResponse<Object> submit(String accessToken, String projectToken, String ops) {
        //验证用户是否登录
        String memberId = redisTemplate.opsForValue().get(accessToken);
        if (StringUtils.isEmpty(memberId)) {
            return AppResponse.fail("用户未登录,无此权限");
        }
        //根据项目标识projectToken取出项目信息
        String jsonString = redisTemplate.opsForValue().get(ProjectConstant.TEMP_PROJECT_PREFIX + projectToken);
        ProjectRedisStorageVo storageVo = JSON.parseObject(jsonString, ProjectRedisStorageVo.class);

        //判断用户操作类型是否是空
        if (!StringUtils.isEmpty(ops)) {
            //判断类型是1,提交审核
            if (ops.equals("1")) {
                //获取项目状态提交枚举
                ProjectStatusEnume submitAuth = ProjectStatusEnume.SUBMIT_AUTH;
                //保存项目信息
                projectCreateService.saveProjectInfo(submitAuth, storageVo);
                return AppResponse.ok(null);

            } else if (ops.equals("0")) {
                //获取项目草稿状态
                ProjectStatusEnume draft = ProjectStatusEnume.DRAFT;
                projectCreateService.saveProjectInfo(draft, storageVo);
                return AppResponse.ok(null);
            } else {
                AppResponse<Object> fail = AppResponse.fail(null);
                fail.setMsg("不支持此操作");
                return fail;
            }
        }
        return AppResponse.fail(null);


    }

    }
