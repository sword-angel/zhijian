package com.offcn.vo;

import lombok.Data;

/**
 * @Auther:Justc
 * @Date:2021/5/14 13:54
 * @Description
 */
@Data
public class BaseVo {
    private String accessToken;
}
