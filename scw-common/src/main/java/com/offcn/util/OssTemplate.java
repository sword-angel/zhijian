package com.offcn.util;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSClientBuilder;
import lombok.Data;
import lombok.ToString;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Auther:Justc
 * @Date:2021/5/13 15:13
 * @Description
 */
@Data
@ToString
public class OssTemplate {
    private String endpoint;
    private String bucketDomain;
    private String accessKeyId;
    private String accessKeySecret;
    private String bucketName;

    public String upload(InputStream inputStream, String fileName) {
        //1.加工文件夹和文件名
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); //格式化日期
        String folderName = sdf.format(new Date());  //将日期作为文件夹名

        //2. 创建OSSClient实例
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        //3. 上传文件流,指定bucket的名称
        ossClient.putObject(bucketName, "pic/" + folderName + "/" + fileName, inputStream);

        //4. 关闭流
        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ossClient.shutdown();
        String url = "https://" + bucketDomain + "/pic/" + folderName + "/" + fileName;
        System.out.println("上传文件访问路径:" + url);
        return url;
    }

}
